# Première étape : Construction de l'application
FROM node:20-alpine3.18 AS build

# Définir le répertoire de travail
WORKDIR /app

# Copier le fichier package.json
COPY package.json .

# Installer les dépendances
RUN npm install

# Copier le reste des fichiers de l'application
COPY . .

# Exécuter la commande de build
RUN npm run build

# Deuxième étape : Configuration de NGINX pour servir les fichiers statiques
FROM nginx:stable

# Copier le fichier nginx.conf local dans le conteneur
COPY docker/nginx.conf /etc/nginx/nginx.conf

# Copier les fichiers construits de la première étape
COPY --from=build /app/build /usr/share/nginx/html

# Exposer le port 80
EXPOSE 80

# Commande pour démarrer NGINX
CMD ["nginx", "-g", "daemon off;"]
